from fastapi import FastAPI, Query, Depends
from typing import Optional
from datetime import date
from pydantic import BaseModel

app = FastAPI()

class SHotel(BaseModel):
    address: str
    name: str
    stars: int

class HotelSearchArgs:
    def __init__(
            self,
            location: str,
            date_from: date, #yyyy-mm-dd
            date_to: date,
            has_spa: Optional[bool] = None,
            stars: Optional[int] = Query(None, ge=1, le=5)
            ) -> None:
        self.location = location
        self.date_from = date_from
        self.date_to = date_to
        self.has_spa = has_spa
        self.stars = stars

@app.get('/hotels')
def get_hotels(
    search_args : HotelSearchArgs = Depends()
) -> list[SHotel]:
    hotels = [
        {
            'address': 'ул. Строителей 12',
            'name': 'отель Пушкина',
            'stars': 5,
        },
    ]
    return hotels

class SBooking(BaseModel):
    room_id: int
    date_from: date #yyyy-mm-dd
    date_to: date

@app.post('/booking')
def add_booking(booking: SBooking):
    pass